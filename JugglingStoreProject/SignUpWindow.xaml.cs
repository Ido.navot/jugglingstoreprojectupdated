﻿using JugglingStoreProject.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
        public SignUpWindow()
        {
            InitializeComponent();
        }

        private void SignUpBtn_Click(object sender, RoutedEventArgs e)
        {
            bool isAdded = false;
            if (userNameTxt.Text != "" && PasswordTxt.Password != "" && firstNameTxt.Text != "" && lastNameTxt.Text != "" && addressTxt.Text != "")
            {
                isAdded = new CustomerDataAccess().AddCustomer(userNameTxt.Text, PasswordTxt.Password, firstNameTxt.Text, lastNameTxt.Text, addressTxt.Text, false);
                if (isAdded == true)
                {
                    signUpResult.Text = "Signing up finished successfully";
                    this.Close();
                }
                else
                {
                    signUpResult.Text = "Something went wrong, try again";
                }
            }
            else
            {
                signUpResult.Text = "Something went wrong, try again";
            }

        }
    }
}
