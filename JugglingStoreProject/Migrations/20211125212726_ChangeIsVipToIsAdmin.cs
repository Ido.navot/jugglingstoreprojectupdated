﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JugglingStoreProject.Migrations
{
    public partial class ChangeIsVipToIsAdmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsVip",
                table: "Customers",
                newName: "IsAdmin");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsAdmin",
                table: "Customers",
                newName: "IsVip");
        }
    }
}
