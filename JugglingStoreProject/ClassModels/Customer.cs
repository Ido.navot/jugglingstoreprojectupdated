﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using JugglingStoreProject.DataAccess;

namespace JugglingStoreProject.ClassModels
{
    public class Customer 
    {
        public Customer()
        {
            this.Orders = new List<Order>();
        }
        
        //class Props:
        [Key]
        public int CustomerId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        // establishes one to many relationship to orders.
        public virtual ICollection<Order> Orders { get; set; }    
    }

    public class class1
    {
    }
}
