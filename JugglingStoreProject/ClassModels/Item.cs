﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using JugglingStoreProject.ClassModels;

namespace JugglingStoreProject
{
     public class Item
    {
        public Item()
        {
            this.Orders = new List<Order>();
        }
        //class Props:
        [Key]
        public int ItemId { get; set; }
        [Required]
        public string ItemName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public ColorPick Color { get; set; }
        // establish many to many realtionship to orders.
        public virtual ICollection<Order> Orders { get; set; }
    }

    public enum ColorPick
    {
        Red =1, 
        Black =2,
        White =3,
        Blue =4
    }
}
