﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using JugglingStoreProject.DataAccess;

namespace JugglingStoreProject.ClassModels
{
    public class Order
    {
        public Order()
        {
            this.Items = new List<Item>();
        }
        [Key]
        public int OrderId { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public DateTime ShipmentDate { get; set; }
        [Required]
        [ForeignKey("Customer_Id")]
        public int CustomerId { get; set; }
        [Required]
        // establishes many to many relationship to Items.
        public virtual ICollection<Item> Items { get; set; }
        
        [NotMapped]
        public double TotalPrice 
        { 
            get
            { 
                if( new CustomerDataAccess().GetCustomerById(CustomerId).IsAdmin)
                {
                    return (this.Items.Sum(i => i.Price)) * 0.80; // discounted price for vip customer.
                }
                else
                {
                    return this.Items.Sum(i => i.Price);// full price for regular customer.
                }   
            } 
        }

    }
}
