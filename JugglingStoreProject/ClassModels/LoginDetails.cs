﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JugglingStoreProject.ClassModels
{
    // Singleton Design Pattern implementation:
    // used to make sure that only 1 user is logged in at a time.
    public class LoginDetails
    {
        private static LoginDetails instance = null;
        public string UserName { get; set; }
        public string Password { get; set; }
        private LoginDetails() { }

        private static object obj = new object();

        public static LoginDetails GetInstnace()
        {
            lock (obj)
            {
                if(LoginDetails.instance == null)
                {
                    instance = new LoginDetails();
                }
                return instance;
            }
        }
    }
}
