﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JugglingStoreProject.ClassModels;
using JugglingStoreProject.DataAccess;
using System.Data;
using System.Globalization;

namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Customer currentCustomer;

        public List<Customer> customers = new CustomerDataAccess().GetAllCustomers();
        public MainWindow()
        {
            InitializeComponent();
            
            
            TotalPriceBlock.Visibility = Visibility.Hidden;
             var loggedInCustomer = LoginDetails.GetInstnace();
            currentCustomer = customers.Find(c => c.UserName == loggedInCustomer.UserName && c.Password == loggedInCustomer.Password);

            if (currentCustomer.IsAdmin == true)
            {
                List<Order> allOrders = OrderDataAccess.GetAllOrders();

                LoadAllOrdersBtn.Visibility = Visibility.Visible;

                totalCustomersCount.Text = $"Total Customers: {customers.Count()}";
                totalOrdersCount.Text = $"Total Orders: {allOrders.Count()}";
            }
            else
            {
                LoadAllOrdersBtn.Visibility = Visibility.Hidden;
                totalCustomersCount.Text = "";
                totalOrdersCount.Text = "";
            }

            DataContext = this;
        }


        private void LoadPersonalOrders_Click_1(object sender, RoutedEventArgs e)
        {

            var PersonalOrders = new OrderDataAccess().GetPersonalOrders(currentCustomer.CustomerId);
            DataGrid1.ItemsSource = PersonalOrders;

            TotalPriceBlock.Visibility = Visibility.Hidden;
        }


        private void NewOrder_Click(object sender, RoutedEventArgs e)
        {
            TotalPriceBlock.Visibility = Visibility.Hidden;

            Window1 CreateOrderWindow = new Window1();
            CreateOrderWindow.ShowDialog();
        }

        private void viewOrderById_Click(object sender, RoutedEventArgs e)
        {


            var selectedOrder = OrderDataAccess.GetOrderByID(int.Parse(OrderIdTextBox.Text));

            TotalPriceBlock.Visibility = Visibility.Visible;
            TotalPriceBlock.Text = $"Order Total Price: {selectedOrder.TotalPrice}";
            DataGrid1.ItemsSource = selectedOrder.Items;
            CollectionViewSource.GetDefaultView(DataGrid1.ItemsSource).Refresh();
        }

        private void filterOrdersByDates_Click(object sender, RoutedEventArgs e)
        {
            TotalPriceBlock.Visibility = Visibility.Hidden;

            int customerId = currentCustomer.CustomerId;

            DateTime StartDate = startDatePicker.SelectedDate.Value;

            DateTime endDate = endDatePicker.SelectedDate.Value;

            DataGrid1.ItemsSource = OrderDataAccess.GetOrdersBetweenDates(customerId, StartDate, endDate);
            CollectionViewSource.GetDefaultView(DataGrid1.ItemsSource).Refresh();


        }

        private void quitMain_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoadAllOrdersBtn_Click(object sender, RoutedEventArgs e)
        {
            TotalPriceBlock.Visibility = Visibility.Hidden;

            DataGrid1.ItemsSource =  OrderDataAccess.GetAllOrders();
            CollectionViewSource.GetDefaultView(DataGrid1.ItemsSource).Refresh();
        }
    }
}
