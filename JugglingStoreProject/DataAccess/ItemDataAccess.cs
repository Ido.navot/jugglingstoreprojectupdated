﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JugglingStoreProject.DataAccess
{
    public class ItemDataAccess
    {
        public void PopulateItemTable() // method to populate Db with items records.
        {
            Item staff = new Item()
            {
                ItemName = "staff",
                Description = "simple contact staff - suitable for begginers",
                Price = 220,
                Color = ColorPick.White,
            };
            Item jugglingBalls = new Item()
            {
                ItemName = "juggling balls",
                Description = "simple light weight juggling balls - suitable for begginers",
                Price = 100,
                Color = ColorPick.Black,
            };
            Item poi = new Item()
            {
                ItemName = "poi",
                Description = "simple poi - suitable for advanced jugglers",
                Price = 180,
                Color = ColorPick.Black,
            };
            Item flowerSticks = new Item()
            {
                ItemName = "flower sticks",
                Description = "simple flower sticks - suitable for begginers",
                Price = 150,
                Color = ColorPick.Red,
            };
            Item jugglingSword = new Item()
            {
                ItemName = "juggling sword",
                Description = "simple contact juggling sword",
                Price = 160,
                Color = ColorPick.Red,
            };
            Item ropeDart = new Item()
            {
                ItemName = "rope dart",
                Description = "simple contact rope dart",
                Price = 130,
                Color = ColorPick.Black,
            };

            using (var dbContext = new JugglingStoreContext())
            {
                dbContext.Items.Add(staff);
                dbContext.Items.Add(jugglingBalls);
                dbContext.Items.Add(flowerSticks);
                dbContext.Items.Add(poi);
                dbContext.Items.Add(ropeDart);
                dbContext.Items.Add(jugglingSword);

                dbContext.SaveChanges();
            }
        }

    }
}
