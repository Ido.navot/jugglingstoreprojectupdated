﻿using JugglingStoreProject.ClassModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static JugglingStoreProject.DataAccess.CustomerDataAccess;


namespace JugglingStoreProject.DataAccess
{
    public class CustomerDataAccess : ICustomer
    {
        // Customer data Methods:

        public List<Customer> GetAllCustomers()
        {
            using (var dbContext = new JugglingStoreContext())
            {
                var customers = dbContext.Customers.ToList();
                return customers;
            }
        }

        public bool AddCustomer(string userName, string password, string firstName, string lastName, string address, bool isVip)
        {
            Customer customer = new Customer()
            {
                UserName = userName,
                FirstName = firstName,
                LastName = lastName,
                Password = password,
                Address = address,
                IsAdmin = isVip
            };

            using (var dbContext = new JugglingStoreContext())
            {
                if (dbContext.Customers.Contains(customer) || dbContext.Customers.Any(c => c.UserName == userName) || dbContext.Customers.Any(c => c.Password == password))
                {
                    return false;
                }
                else
                {
                    dbContext.Customers.Add(customer);
                    dbContext.SaveChanges();
                    return true;
                }
            }
        }

        public Customer GetCustomerById(int id)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                var customer = dbContext.Customers.Find(id);
                return customer;
            }
        }

        public void PopulateCustomerTable() // method to populate Db with records.
        {
            AddCustomer("IdoNavot303", "12312", "Ido", "Navot", "Haifa, Neve shaanan, Pinsker 27", true);
            AddCustomer("AviArnavi999", "100452", "Avi", "Levy", "Beer sheva, rabin 72", false);
            AddCustomer("Dandi88", "972310", "Daniel", "Cohen", "Tel aviv, Rothchild 80", false);
            AddCustomer("YoavDude", "61259", "Yoav", "Pintel", "Sderot, Hanavi 14", true);
            AddCustomer("Eliko245", "114425", "Eli", "Laban", "Haifa, Hadar 66", false);
            AddCustomer("SuperNiro", "762390", "Nir", "Barak", "Yokneam, Moshe dayan 25", false);
        }


    }
    public interface ICustomer
    {
        abstract List<Customer> GetAllCustomers();
        abstract bool AddCustomer(string userName, string password, string firstName, string lastName, string address, bool isVip);
        abstract Customer GetCustomerById(int id);
    }
}

