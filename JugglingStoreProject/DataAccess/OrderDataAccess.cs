﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JugglingStoreProject.ClassModels;
using Microsoft.EntityFrameworkCore;

namespace JugglingStoreProject.DataAccess
{
    public class OrderDataAccess // Orders data methods:
    {
        public static void AddOrder(Order order)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                dbContext.Add(order);
                dbContext.SaveChanges();
            }
        }

        public List<Order> GetPersonalOrders(int id)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                var personalOrders = dbContext.Orders.Where(o => o.CustomerId == id).ToList();
                return personalOrders;
            }
        }
        public static List<Order> GetAllOrders()
        {
            var orders = new List<Order>();
            using (var dbContext = new JugglingStoreContext())
            {
                orders = dbContext.Orders.ToList();
            }
            return orders;
        }
        public static Order GetOrderByID(int id)
        {
            var order = new Order();
            var items = new List<Item>();
            using (var dbContext = new JugglingStoreContext())
            {
                order = dbContext.Orders.Include(o => o.Items).Single(o => o.OrderId == id);
            }
            return order;
        }

        public static List<Order> GetOrdersBetweenDates(int customerId, DateTime start, DateTime end)
        {
            var orders = new List<Order>();
            using (var dbContext = new JugglingStoreContext())
            {
                orders = dbContext.Orders.Where(o => (o.OrderDate >= start) && (o.OrderDate <= end) && (o.CustomerId == customerId)).ToList();
            }
            return orders;
        }

        public static void PopulateOrderTable() // method to poulate the Db with Orders records.
        {
            var initOrders = new List<Order>();
            using (var dbContext = new JugglingStoreContext())
            {
                Item staff = dbContext.Items.FirstOrDefault(i => i.ItemName == "staff");
                Item jugglingBalls = dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling balls");
                Item poi = dbContext.Items.FirstOrDefault(i => i.ItemName == "poi");
                Item flowerSticks = dbContext.Items.FirstOrDefault(i => i.ItemName == "flower sticks");
                Item jugglingSword = dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling sword");
                Item ropeDart = dbContext.Items.FirstOrDefault(i => i.ItemName == "rope dart");

                var customers = new CustomerDataAccess().GetAllCustomers();

                if (dbContext.Orders.Count() == 0)
                {


                    // change date foramt on all orders.
                    Order order1 = new Order()
                    {
                        CustomerId = customers[0].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/06/01", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/06", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        staff, poi,ropeDart
                    }
                    };

                    Order order2 = new Order()
                    {
                        CustomerId = customers[0].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/05/11", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/05/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        staff, poi,ropeDart, flowerSticks, jugglingBalls
                    }
                    };

                    Order order3 = new Order()
                    {
                        CustomerId = customers[0].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/08/11", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        staff
                    }
                    };

                    Order order4 = new Order()
                    {
                        CustomerId = customers[0].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/08/10", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/15", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        poi,jugglingSword
                    }
                    };

                    Order order5 = new Order()
                    {
                        CustomerId = customers[0].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/10/12", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/10/17", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls
                    }
                    };

                    Order order6 = new Order()
                    {
                        CustomerId = customers[1].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/03/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/03/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        poi,ropeDart
                    }
                    };

                    Order order7 = new Order()
                    {
                        CustomerId = customers[1].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/04/09", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/04/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls
                    }
                    };

                    Order order8 = new Order()
                    {
                        CustomerId = customers[1].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/06/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        staff, flowerSticks
                    }
                    };

                    Order order9 = new Order()
                    {
                        CustomerId = customers[1].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/08/10", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/17", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        poi,jugglingSword, staff, jugglingSword, jugglingBalls
                    }
                    };

                    Order order10 = new Order()
                    {
                        CustomerId = customers[1].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/11/12", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/11/19", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, flowerSticks
                    }
                    };

                    Order order11 = new Order()
                    {
                        CustomerId = customers[2].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/03/20", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/03/27", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        poi,ropeDart, jugglingSword
                    }
                    };

                    Order order12 = new Order()
                    {
                        CustomerId = customers[2].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/04/19", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/04/26", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls
                    }
                    };

                    Order order13 = new Order()
                    {
                        CustomerId = customers[2].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/06/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/23", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        staff, flowerSticks, flowerSticks, flowerSticks
                    }
                    };

                    Order order14 = new Order()
                    {
                        CustomerId = customers[2].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/08/13", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/20", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        poi,jugglingSword, staff
                    }
                    };

                    Order order15 = new Order()
                    {
                        CustomerId = customers[2].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/01/01", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/01/08", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, flowerSticks, jugglingBalls, jugglingSword
                    }
                    };

                    Order order16 = new Order()
                    {
                        CustomerId = customers[3].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/04/11", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/04/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        poi, flowerSticks, jugglingBalls, jugglingSword
                    }
                    };

                    Order order17 = new Order()
                    {
                        CustomerId = customers[3].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/05/17", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/05/22", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, flowerSticks, jugglingBalls, jugglingSword, staff, staff
                    }
                    };

                    Order order18 = new Order()
                    {
                        CustomerId = customers[3].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/06/13", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/18", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, jugglingBalls, jugglingSword
                    }
                    };

                    Order order19 = new Order()
                    {
                        CustomerId = customers[3].CustomerId,
                        OrderDate = DateTime.ParseExact("2021/08/02", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/07", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        poi, flowerSticks, ropeDart
                    }
                    };

                    Order order20 = new Order()
                    {
                        CustomerId = customers[3].CustomerId,
                        OrderDate = DateTime.ParseExact("2020/02/23", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/02/28", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, jugglingSword
                    }
                    };

                    Order order21 = new Order()
                    {
                        CustomerId = customers[4].CustomerId,
                        OrderDate = DateTime.ParseExact("2019/03/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/03/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, flowerSticks, jugglingBalls
                    }
                    };

                    Order order22 = new Order()
                    {
                        CustomerId = customers[4].CustomerId,
                        OrderDate = DateTime.ParseExact("2019/06/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/06/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingSword, staff, staff, staff
                    }
                    };

                    Order order23 = new Order()
                    {
                        CustomerId = customers[4].CustomerId,
                        OrderDate = DateTime.ParseExact("2019/11/22", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/11/29", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, flowerSticks, jugglingBalls, jugglingSword, staff
                    }
                    };

                    Order order24 = new Order()
                    {
                        CustomerId = customers[4].CustomerId,
                        OrderDate = DateTime.ParseExact("2019/01/01", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/01/08", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        flowerSticks
                    }
                    };

                    Order order25 = new Order()
                    {
                        CustomerId = customers[4].CustomerId,
                        OrderDate = DateTime.ParseExact("2019/12/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/12/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, staff, poi, poi
                    }
                    };

                    Order order26 = new Order()
                    {
                        CustomerId = customers[5].CustomerId,
                        OrderDate = DateTime.ParseExact("2020/12/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/12/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, staff
                    }
                    };

                    Order order27 = new Order()
                    {
                        CustomerId = customers[5].CustomerId,
                        OrderDate = DateTime.ParseExact("2020/03/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/03/23", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi,staff, jugglingSword
                    }
                    };

                    Order order28 = new Order()
                    {
                        CustomerId = customers[5].CustomerId,
                        OrderDate = DateTime.ParseExact("2020/01/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/01/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, staff, ropeDart, flowerSticks
                    }
                    };

                    Order order29 = new Order()
                    {
                        CustomerId = customers[5].CustomerId,
                        OrderDate = DateTime.ParseExact("2020/03/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/03/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, staff, flowerSticks
                    }
                    };

                    Order order30 = new Order()
                    {
                        CustomerId = customers[5].CustomerId,
                        OrderDate = DateTime.ParseExact("2019/12/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/12/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<Item>()
                    {
                        jugglingBalls, poi, staff, poi, poi
                    }
                    };



                    initOrders.Add(order1);
                    initOrders.Add(order2);
                    initOrders.Add(order3);
                    initOrders.Add(order4);
                    initOrders.Add(order5);
                    initOrders.Add(order6);
                    initOrders.Add(order7);
                    initOrders.Add(order8);
                    initOrders.Add(order9);
                    initOrders.Add(order10);
                    initOrders.Add(order11);
                    initOrders.Add(order12);
                    initOrders.Add(order13);
                    initOrders.Add(order14);
                    initOrders.Add(order15);
                    initOrders.Add(order16);
                    initOrders.Add(order17);
                    initOrders.Add(order18);
                    initOrders.Add(order19);
                    initOrders.Add(order20);
                    initOrders.Add(order21);
                    initOrders.Add(order21);
                    initOrders.Add(order22);
                    initOrders.Add(order23);
                    initOrders.Add(order24);
                    initOrders.Add(order25);
                    initOrders.Add(order26);
                    initOrders.Add(order27);
                    initOrders.Add(order28);
                    initOrders.Add(order29);
                    initOrders.Add(order30);


                    //Customer 1:

                    //customers[0].Orders.Add(order1);  
                    //customers[0].Orders.Add(order2); 
                    //customers[0].Orders.Add(order3);
                    //customers[0].Orders.Add(order4);
                    //customers[0].Orders.Add(order5);

                    ////customer 2:

                    //customers[1].Orders.Add(order6);
                    //customers[1].Orders.Add(order7);
                    //customers[1].Orders.Add(order8);
                    //customers[1].Orders.Add(order9);
                    //customers[1].Orders.Add(order10);

                    ////customer 3:

                    //customers[2].Orders.Add(order11);
                    //customers[2].Orders.Add(order12);
                    //customers[2].Orders.Add(order13);
                    //customers[2].Orders.Add(order14);
                    //customers[2].Orders.Add(order15);

                    ////customer 4:

                    //customers[3].Orders.Add(order16);
                    //customers[3].Orders.Add(order17);
                    //customers[3].Orders.Add(order18);
                    //customers[3].Orders.Add(order19);
                    //customers[3].Orders.Add(order20);

                    ////customer 5:

                    //customers[4].Orders.Add(order21);
                    //customers[4].Orders.Add(order22);
                    //customers[4].Orders.Add(order23);
                    //customers[4].Orders.Add(order24);
                    //customers[4].Orders.Add(order25);

                    ////customer 6:

                    //customers[5].Orders.Add(order26);
                    //customers[5].Orders.Add(order27);
                    //customers[5].Orders.Add(order28);
                    //customers[5].Orders.Add(order29);
                    //customers[5].Orders.Add(order30);

                    // final db actions:

                    dbContext.AddRange(initOrders);// add each order seperatly
                    dbContext.SaveChanges();
                }
            };            
        }


    }
}

