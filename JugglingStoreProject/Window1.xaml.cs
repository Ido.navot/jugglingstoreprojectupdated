﻿using JugglingStoreProject.ClassModels;
using JugglingStoreProject.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public static List<Item> CurrentItems = new List<Item>();
        public static Order currentOrder;
        public List<Customer> customers = new CustomerDataAccess().GetAllCustomers();
        public static Customer currentCustomer;
        public Window1()
        {
            InitializeComponent();

            var loggedInCustomer = LoginDetails.GetInstnace();

            currentCustomer = customers.Find(c => c.UserName == loggedInCustomer.UserName && c.Password == loggedInCustomer.Password);
            if (currentCustomer.Orders == null)
            {
                currentCustomer.Orders = new List<Order>();
            }
            currentOrder = new Order();
            currentOrder.Items = new List<Item>();
            ItemPickedGrid.ItemsSource = currentOrder.Items;
        }

        private void AddStaffToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item staff = dbContext.Items.FirstOrDefault(i => i.ItemName == "staff");
                
                currentOrder.Items.Add(staff);
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddBallsToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item jugglingBalls = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling balls");
                currentOrder.Items.Add(jugglingBalls);
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddPoiToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item poi = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "poi");
                currentOrder.Items.Add(poi);
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddFlowerToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item flowerSticks = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "flower sticks");
                currentOrder.Items.Add(flowerSticks);
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddJugglingSwordToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item jugglingSword = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling sword");
                currentOrder.Items.Add(jugglingSword);
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddRopeDartToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item ropeDart = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "rope dart");
                currentOrder.Items.Add(ropeDart);
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void SendOrder_Click(object sender, RoutedEventArgs e)
        {

            if (currentCustomer.Orders == null)
            {
                currentCustomer.Orders = new List<Order>();
            }


            using (var dbContext = new JugglingStoreContext())
            {
                currentOrder.CustomerId = currentCustomer.CustomerId;
                currentOrder.OrderDate = DateTime.Now;

                if (currentCustomer.IsAdmin == true)
                {
                    currentOrder.ShipmentDate = DateTime.Now.AddDays(5); // shorter delivery time for admins.
                }
                else
                {
                    currentOrder.ShipmentDate = DateTime.Now.AddDays(7);
                }
                
                currentOrder.Items = (ICollection<Item>)ItemPickedGrid.ItemsSource;
                currentCustomer.Orders.Add(currentOrder);

                dbContext.Update(currentCustomer);
                dbContext.Add(currentOrder);
                dbContext.SaveChanges();

                dbContext.SaveChanges();
            }

            this.Close();
        }

        private void quitMain_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

